<?php
/*
Plugin Name: Edudemia News Addon
Plugin URI: 
Description: Add news management to your DMS
Version: 0.4.3
Author: Tyler Pruitt
Author URI:
Bitbucket Plugin URI: greatyler/edudemia-news
Bitbucket Branch: realmaster

Credits:
This plugin's director of development and primary developer is Tyler Pruitt.

The following plugins were sourced or referrenced in this project:
Code from:



Dependencies or Referrences:





License

Edudemia is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
any later version.

Edudemia software is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
 
See http://www.gnu.org/licenses/old_licenses/gpl_2.0.en.html.
*/

include(dirname( __FILE__ ) . '/core.php');
include(dirname( __FILE__ ) . '/news_menu_page.php');
include(dirname( __FILE__ ) . '/custom-fields-news.php');
include(dirname( __FILE__ ) . '/taxonomies.php');
include(dirname( __FILE__ ) . '/shortcode.php');
include(dirname( __FILE__ ) . '/widget.php');
include(dirname( __FILE__ ) . '/plugins.php');
include(dirname( __FILE__ ) . '/css/css_menu_page.php');
//include(dirname( __FILE__ ) . '/single-news.php');
include(dirname( __FILE__ ) . '/misc_tools.php');


//Activation hooks
register_activation_hook( __FILE__, 'edudms_news_activate' );

function edudms_news_activate() {
	
}


function edudms_news_css_registration() {
wp_register_style('edudms_news', plugins_url('css/news.css',__FILE__ ));
wp_enqueue_style('edudms_news');

}
add_action( 'wp_enqueue_scripts','edudms_news_css_registration');




//Add News Templates


function edudms_news_submenu_settings() {
add_submenu_page('options-general.php', 'News Settings', 'EduDMS News', 'manage_options', 'event_settings', 'edudms_news_menu_render');
}
add_action("admin_menu", 'edudms_news_submenu_settings');



// Register Custom Post Type
function edudms_news_create_news_post_type() {

	$labels = array(
		'name'                  => _x( 'News', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'News', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'News', 'text_domain' ),
		'name_admin_bar'        => __( 'News', 'text_domain' ),
		'archives'              => __( 'Item Archives', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
		'all_items'             => __( 'All Items', 'text_domain' ),
		'add_new_item'          => __( 'Add New Item', 'text_domain' ),
		'add_new'               => __( 'Add News Post', 'text_domain' ),
		'new_item'              => __( 'New Item', 'text_domain' ),
		'edit_item'             => __( 'Edit Item', 'text_domain' ),
		'update_item'           => __( 'Update Item', 'text_domain' ),
		'view_item'             => __( 'View Item', 'text_domain' ),
		'search_items'          => __( 'Search Item', 'text_domain' ),
		'not_found'             => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Featured Image', 'text_domain' ),
		'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
		'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
		'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
		'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
		'items_list'            => __( 'Items list', 'text_domain' ),
		'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter news list', 'text_domain' ),
	);
	$capabilities = array(
		'edit_post'             => 'edit_news',
		'read_post'             => 'read_news',
		'delete_post'           => 'delete_news',
		'edit_posts'            => 'edit_news',
		'edit_others_posts'     => 'edit_others_news',
		'publish_posts'         => 'publish_news',
		'read_private_posts'    => 'read_private_news',
	);
	$args = array(
		'label'                 => __( 'News', 'text_domain' ),
		'description'           => __( 'All your department\'s news stories!', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'revisions', ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 6,
		'menu_icon'             => 'dashicons-media-document',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,		
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		//'capabilities'          => $capabilities,
	);
	register_post_type( 'news', $args );

}
add_action( 'init', 'edudms_news_create_news_post_type', 0 );

	




//add news template

function add_news_template( $single_template )
{
	$object = get_queried_object();
	$single_postType_postName_template = locate_template(dirname(__FILE__) . "/single-news.php");
	if( file_exists( $single_postType_postName_template ) )
	{
		return $single_postType_postName_template;
	} else {
		return $single_template;
	}
}
add_filter( 'single_template', 'add_news_template', 10, 1 );


function get_news_template($single_template) {
     global $post;

     if ($post->post_type == 'news') {
          $single_template = dirname( __FILE__ ) . '/single-news.php';
     }
     return $single_template;
}
add_filter( 'single_template', 'get_news_template' );

















?>