<?php


if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_news-setup',
		'title' => 'News setup',
		'fields' => array (
			array (
				'key' => 'field_5783dd54e27b1',
				'label' => 'Required Information',
				'name' => '',
				'type' => 'tab',
			),
			array (
				'key' => 'field_5783dd86e27b2',
				'label' => 'Relevant Date',
				'name' => 'relevant_date',
				'type' => 'date_picker',
				'instructions' => 'Please enter a date on which this news occurred or which is close to the time that this news took place.',
				'required' => 1,
				'date_format' => 'yymmdd',
				'display_format' => 'dd/mm/yy',
				'first_day' => 1,
			),
			array (
				'key' => 'field_5783deb198a37',
				'label' => 'Source',
				'name' => 'news_source',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_5783ddb0fde8c',
				'label' => 'Additional Information',
				'name' => '',
				'type' => 'tab',
			),
			array (
				'key' => 'field_5783ddc1fde8d',
				'label' => 'Additional Info and Advanced Fields',
				'name' => 'additional_info_and_advanced_fields',
				'type' => 'checkbox',
				'instructions' => 'Check the boxes of any types of information you want to provide.',
				'choices' => array (
					'link_to' => 'Link to full story',
					'contact' => 'Contact for additional information',
				),
				'default_value' => '',
				'layout' => 'horizontal',
			),
			array (
				'key' => 'field_5783de2ddeebb',
				'label' => 'Contact Name',
				'name' => 'contact_name',
				'type' => 'text',
				'instructions' => 'Enter the name of the person you would like viewers to contact for more information.',
				'conditional_logic' => array (
					'status' => 1,
					'rules' => array (
						array (
							'field' => 'field_5783ddc1fde8d',
							'operator' => '==',
							'value' => 'contact',
						),
					),
					'allorany' => 'all',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_5783de47deebc',
				'label' => 'Contact Email',
				'name' => 'contact_email',
				'type' => 'email',
				'conditional_logic' => array (
					'status' => 1,
					'rules' => array (
						array (
							'field' => 'field_5783ddc1fde8d',
							'operator' => '==',
							'value' => 'contact',
						),
					),
					'allorany' => 'all',
				),
				'default_value' => '',
				'placeholder' => 'someone@somewhere.edu',
				'prepend' => '',
				'append' => '',
			),
			array (
				'key' => 'field_5783de771cae8',
				'label' => 'Link to URL',
				'name' => 'link_to_url',
				'type' => 'text',
				'instructions' => 'Enter the URL you would like to link to.
	
	This may be different from the "Source" above if you choose to cite the Source in any way other than a URL to another online story. This URL might link to a place where a physical publication can be purchased, a collection of works might be viewed, or some other situation.',
				'conditional_logic' => array (
					'status' => 1,
					'rules' => array (
						array (
							'field' => 'field_5783ddc1fde8d',
							'operator' => '==',
							'value' => 'link_to',
						),
					),
					'allorany' => 'all',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'news',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}






?>