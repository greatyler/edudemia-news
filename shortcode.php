<?php



add_shortcode ('news', 'edudms_news_shortcode');
add_shortcode ('news_widget', 'edudms_news_show');


function edudms_news_show($atts) {
	
	$shortcode_atts = shortcode_atts( array(
        'limit' => '3',
		'type' => 'all',
		'style' => 'default',
		'layout' => 'page',
		'after-date' => '20160101',
		'before-date' => '20200101'
    ), $atts );

	$earliestdate = $shortcode_atts['after-date'];
	$tomorrow = date( 'Ymd' ) + 1;
	$latestdate = $shortcode_atts['before-date'];
	$limit = $shortcode_atts['limit'];
	
	$news_stories = edudms_news_get( $earliestdate, $latestdate, $limit);
	

foreach ( $news_stories as $ns ) {
	$title = $ns->post_title;
	$content = $ns->post_content;
	$this_post_id = $ns->ID;
	
	
	$raw_relevant_date = get_field('relevant_date', $this_post_id);
	$relevant_date = new DateTime($raw_relevant_date);
	$source = get_field('news_source', $this_post_id);
	
	?>
	<div class="edudms edudms_news block <?php echo $shortcode_atts['layout'];?>">
	<div class="edudms edudms_news pic <?php echo $shortcode_atts['layout'];?>"><a href="<?php echo get_post_permalink($this_post_id); ?>"><?php $testimage = get_the_post_thumbnail($this_post_id, 'thumbnail' ); echo $testimage; ?></a></div>
	<div class="edudms edudms_news title <?php echo $shortcode_atts['layout'];?>"><a href="<?php echo get_post_permalink($this_post_id); ?>"><?php echo $title ?></a></div>
	<div class="edudms edudms_news date <?php echo $shortcode_atts['layout'];?>"><?php echo $relevant_date->format('F j Y'); ?></div>
	<div class="edudms edudms_news content <?php echo $shortcode_atts['layout'];?>"><?php echo $content ?></div>
	
	<div class="edudms edudms_news source <?php echo $shortcode_atts['layout'];?>"><?php echo $source; ?></div>
	</div>
	<?php
	
	
}

	
	
}




function edudms_news_shortcode($atts) {
ob_start();
edudms_news_show($atts);
$news_buffer = ob_get_clean();
	return $news_buffer;
}



?>