<?php



//add_action( 'plugins_loaded', 'copy_publish_date_into_rel_date' );

function copy_publish_date_into_rel_date() {
	
	$args = array( 'post_type' => 'news' );
	
	$news_posts = get_posts($args);
	
	foreach($news_posts as $news_post) {
		
		update_field( 'relevant_date', $news_post->post_date, $news_post->ID );
			
	}
	
}



?>